#!/usr/bin/env bash

if [[ $1 == "-h" || $1 == "--help" ]]; then
  echo "Usage: $0 [-f|--fill]"
  exit 1
fi

composer install
database/propel sql:build --config-dir=database --schema-dir=database --output-dir=database/generated-sql --overwrite
database/propel model:build --config-dir=database --schema-dir=database --output-dir=database/generated-classes
database/propel sql:insert --config-dir=database --sql-dir=database/generated-sql
database/propel config:convert --config-dir=database --output-dir=database/generated-conf
composer dump-autoload

if [[ $1 == "-f" || $1 == "--fill" ]]; then
  php database/fill-database.php
fi

exit 0;
