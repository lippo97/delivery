<?php declare(strict_types = 1);
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/generated-conf/config.php';

$users = require __DIR__ . '/fixtures/users.php';
$categories = require __DIR__ . '/fixtures/categories.php';
$deliveryPoints = require __DIR__ . '/fixtures/deliveryPoints.php';
$articles = require __DIR__ . '/fixtures/articles.php';

print "Setting up users...";
foreach ($users as $userData) {
  $user = new \User();
  $user->setEmail($userData['email']);
  $user->setPassword($userData['password']);
  $user->setSalt($userData['salt']);
  if (isset($userData['administrator']))
    $user->setAdministrator($userData['administrator']);
  if (isset($userData['restaurant']))
    $user->setRestaurant($userData['restaurant']);
  if (isset($userData['restaurant_enabled']))
    $user->setRestaurantEnabled($userData['restaurant_enabled']);
  if (isset($userData['restaurant_name']))
    $user->setRestaurantName($userData['restaurant_name']);
  if (isset($userData['restaurant_tel']))
    $user->setRestaurantTel($userData['restaurant_tel']);
  if (isset($userData['restaurant_address']))
    $user->setRestaurantAddress($userData['restaurant_address']);
  try {
    $user->save();
  } catch (\Propel\Runtime\Exception\PropelException $e) {
  }
}

print " Done. \n";

print "Setting up categories...";
foreach ($categories as $categoryData) {
  $category = new \Category();
  $category->setName($categoryData['name']);
  try {
    $category->save();
  } catch (\Propel\Runtime\Exception\PropelException $e) {
  }
}
print " Done. \n";

print "Setting up delivery points...";
foreach ($deliveryPoints as $deliveryPointData) {
  $deliveryPoint = new \DeliveryPoint();
  $deliveryPoint->setName($deliveryPointData['name']);
  try {
    $deliveryPoint->save();
  } catch (\Propel\Runtime\Exception\PropelException $e) {
  }
}
print " Done. \n";

print "Setting up articles...";
foreach ($articles as $email => $articlesData) {
  $user = \UserQuery::create()->findOneByEmail($email);
  foreach ($articlesData as $articleData) {
    $article = new \Article();
    $article->setName($articleData['name']);
    $article->setDescription($articleData['description']);
    $article->setUser($user);
    $article->setCategoryId($articleData['categoryId']);
    $article->setPrice($articleData['price']);

    try {
      $article->save();
    } catch (\Propel\Runtime\Exception\PropelException $e) {
    }
  }
}
print " Done. \n";
