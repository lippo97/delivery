<?php declare(strict_types = 1);

return [
  "restaurant@delivery.com" => [
    [
      "name" => "Patatine",
      "categoryId" => 1,
      "description" => "Questo è un panino di prova, molto buono e cucinato con amore.",
      "price" => 3.50
    ], [
      "name" => "BBq",
      "categoryId" => 2,
      "description" => "Questo è un panino di prova, più buono di quello di prima.",
      "price" => 3.50
    ], [
      "name" => "Carne arrosto",
      "categoryId" => 3,
      "description" => "Saporita e piccante",
      "price" => 3.50
    ]
  ],
  "restaurant2@delivery.com" => [
    [
      "name" => "Insalata",
      "categoryId" => 1,
      "description" => "Insalata con cetrioli",
      "price" => 3.50
    ], [
      "name" => "Piadina salsiccia",
      "categoryId" => 2,
      "description" => "Questo è un panino di prova, più buono di quello di prima.",
      "price" => 3.50
    ], [
      "name" => "Mascarpone",
      "categoryId" => 4,
      "description" => "Questo è un panino di prova, più buono di quello di prima.",
      "price" => 3.50
    ]
  ]
];
