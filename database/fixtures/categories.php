<?php declare(strict_types = 1);

return [
  [
    "name" => "Antipasti"
  ], [
    "name" => "Primi piatti"
  ], [
    "name" => "Secondi piatti"
  ], [
    "name" => "Dessert"
  ], [
    "name" => "Altro"
  ], [
    "name" => "Bibite"
  ]
];
