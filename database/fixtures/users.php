<?php declare(strict_types = 1);

$genericPassword = "delivery";
$genericSalt = "salt";
$genericHashedPassword = hash('sha512', $genericPassword.$genericSalt);

return [
  [
    "email" => "example@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
  ], [
    "email" => "example2@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
  ], [
    "email" => "admin@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
    "administrator" => true
  ], [
    "email" => "restaurant@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
    "restaurant" => true,
    "restaurant_enabled" => true,
    "restaurant_name" => "American",
    "retaurant_tel" => "0541 55352",
    "restaurant_address" => "Cesena, Via Panini 5"
  ], [
    "email" => "restaurant2@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
    "restaurant" => true,
    "restaurant_enabled" => true,
    "restaurant_name" => "Da Peppe",
    "retaurant_tel" => "0541 55353",
    "restaurant_address" => "Cesena, Via Panini 6"
  ], [
    "email" => "restaurant3@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
    "restaurant" => true,
    "restaurant_enabled" => true,
    "restaurant_name" => "Kebab",
    "retaurant_tel" => "0541 55354",
    "restaurant_address" => "Cesena, Via Panini 7"
  ], [
    "email" => "restaurant4@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
    "restaurant" => true,
    "restaurant_enabled" => false,
    "restaurant_name" => "Sushi Tao",
    "retaurant_tel" => "0541 55355",
    "restaurant_address" => "Cesena, Via Panini 8"
  ], [
    "email" => "restaurant5@delivery.com",
    "password" => $genericHashedPassword,
    "salt" => $genericSalt,
    "restaurant" => true,
    "restaurant_enabled" => false,
    "restaurant_name" => "Giro Pizza",
    "retaurant_tel" => "0541 33354",
    "restaurant_address" => "Cesena, Via Panini 11"
  ],
];
