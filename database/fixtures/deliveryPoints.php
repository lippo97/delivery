<?php declare(strict_types = 1);

return [
  [
    "name" => "Aula 2.1"
  ], [
    "name" => "Aula 2.2"
  ], [
    "name" => "Aula 2.3"
  ], [
    "name" => "Aula 2.4"
  ], [
    "name" => "Aula 2.5"
  ], [
    "name" => "Aula 2.6"
  ], [
    "name" => "Aula 2.7"
  ], [
    "name" => "Aula 2.8"
  ], [
    "name" => "Aula 2.9"
  ], [
    "name" => "Aula 2.10"
  ], [
    "name" => "Aula 2.11"
  ], [
    "name" => "Aula 2.12"
  ], [
    "name" => "Laboratorio 3.1"
  ], [
    "name" => "Laboratorio 3.2"
  ], [
    "name" => "Laboratorio 3.4"
  ], [
    "name" => "Aula 3.5"
  ], [
    "name" => "Aula 3.6"
  ], [
    "name" => "Aula 3.7"
  ], [
    "name" => "Aula 3.8"
  ], [
    "name" => "Aula 3.9"
  ], [
    "name" => "Aula 3.10"
  ], [
    "name" => "Aula 3.11"
  ]
];
