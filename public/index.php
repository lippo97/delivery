<?php declare(strict_types = 1);

//echo $_SERVER['REQUEST_URI'];
//echo $_GET['HT_APACHE'];
if (isset($_GET['HT_APACHE'])) {
  $root = str_replace('/public/index.php', '', $_SERVER['PHP_SELF']);
  $trimmed = str_replace($root, '', $_SERVER['REQUEST_URI']);
  $_SERVER['REQUEST_URI'] = $trimmed;
} else {
  $root = "";
}
$GLOBALS['ROOT'] = $root;

define ('SITE_ROOT', realpath(__DIR__ . '/..'));

require __DIR__ . '/../src/Bootstrap.php';
