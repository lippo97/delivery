<?php declare(strict_types = 1);

return [
  ['GET', '/', ['Example\Controllers\Homepage', 'show']],
  ['GET', '/ciao', ['Example\Controllers\Homepage', 'show']],
  ['DELETE', '/', ['Example\Controllers\Homepage', 'show']]
];
