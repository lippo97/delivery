<?php

namespace Delivery\Session;

/**
 * Implementazione di una sessione sicura accessibile tramite un Singleton.
 */
class SecureSession implements Session
{
  protected static $istance = null;
  //protected static $istance = null;

  protected function __construct()
  {
    $session_name = 'sec_session_id';
    $secure = false;
    $httponly = true;
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name);
    session_start();
    session_regenerate_id();
  }

  private function __clone()
  {
  }

  private function __wakeup()
  {
  }

  // Questo consente l'eventuale estensione
  // della classe, utilizza il late binding del costruttore
  public static function getIstance()
  {
    if (null === static::$istance) {
      static::$istance = new static();
    }
    return static::$istance;
  }

  public function has(string $key)
  {
    return isset($_SESSION[$key]);
  }

  public function get(string $key)
  {
    return $_SESSION[$key];
  }

  public function set(string $key, $value)
  {
    $_SESSION[$key] = $value;
  }

  public function remove(string $key)
  {
    unset($_SESSION[$key]);
  }

  public function destroy()
  {
    if (static::$istance !== null) {
      static::$istance = null;
      session_destroy();
    }
  }
}
