<?php

namespace Delivery\Session;

interface Session
{
  public function has(string $key);
  public function get(string $key);
  public function set(string $key, $value);
  public function remove(string $key);
  public function destroy();
}
