<?php declare(strict_types = 1);

namespace Delivery;

require __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);

$environment = 'development';

/**
 * * Register the error handler
 * */
$whoops = new \Whoops\Run;
if ($environment !== 'production') {
  $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
} else {
$whoops->pushHandler(function($e){
  echo 'Todo: Friendly error page and send an email to the developer';
});
}
$whoops->register();

require_once __DIR__ . '/../database/generated-conf/config.php';

$injector = include('Dependencies.php');
//isset($_POST['_method'])

//$request = new \Http\HttpRequest($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
//$response = new \Http\HttpResponse;

$request = $injector->make('Http\HttpRequest');
$response = $injector->make('Http\HttpResponse');

$routeDefinitionCallback = function (\FastRoute\RouteCollector $r) {
  //$routes = include('Routes.php');
  //foreach ($routes as $route) {
    //$r->addRoute($route[0], $route[1], $route[2]);
  //}
  $r->addGroup('/categories', function (\FastRoute\RouteCollector $r) {
    //$r->addRoute('GET', '/{id:\d+}', ['Delivery\Controllers\Categories', 'show']);
    $r->addRoute('GET', '/insert', ['Delivery\Controllers\Categories', 'insert']);
    $r->addRoute('POST', '/', ['Delivery\Controllers\Categories', 'create']);
    $r->addRoute('GET', '/{id:\d+}/edit', ['Delivery\Controllers\Categories', 'edit']);
    $r->addRoute('PUT', '/{id:\d+}', ['Delivery\Controllers\Categories', 'update']);
    $r->addRoute('DELETE', '/{id:\d+}', ['Delivery\Controllers\Categories', 'delete']);
    $r->addRoute('GET', '/', ['Delivery\Controllers\Categories', 'all']);
  });

  $r->addGroup('/delivery_points', function (\FastRoute\RouteCollector $r) {
    //$r->addRoute('GET', '/{id:\d+}', ['Delivery\Controllers\Categories', 'show']);
    $r->addRoute('GET', '/insert', ['Delivery\Controllers\DeliveryPoints', 'insert']);
    $r->addRoute('POST', '/', ['Delivery\Controllers\DeliveryPoints', 'create']);
    $r->addRoute('GET', '/{id:\d+}/edit', ['Delivery\Controllers\DeliveryPoints', 'edit']);
    $r->addRoute('PUT', '/{id:\d+}', ['Delivery\Controllers\DeliveryPoints', 'update']);
    $r->addRoute('DELETE', '/{id:\d+}', ['Delivery\Controllers\DeliveryPoints', 'delete']);
    $r->addRoute('GET', '/', ['Delivery\Controllers\DeliveryPoints', 'all']);
  });

  $r->addGroup('/articles', function (\FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', ['Delivery\Controllers\Articles', 'all']);
    $r->addRoute('GET', '/insert', ['Delivery\Controllers\Articles', 'insert']);
    $r->addRoute('POST', '/', ['Delivery\Controllers\Articles', 'create']);
    $r->addRoute('GET', '/{id:\d+}/edit', ['Delivery\Controllers\Articles', 'edit']);
    $r->addRoute('PUT', '/{id:\d+}', ['Delivery\Controllers\Articles', 'update']);
    $r->addRoute('DELETE', '/{id:\d+}', ['Delivery\Controllers\Articles', 'delete']);
  });

  $r->addGroup('/users', function (\FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/{id:\d+}', ['Delivery\Controllers\Users', 'show']);
    $r->addRoute('GET', '/', ['Delivery\Controllers\Users', 'all']);
    $r->addRoute('GET', '/{id:\d+}/edit', ['Delivery\Controllers\Users', 'edit']);
    $r->addRoute('PUT', '/{id:\d+}', ['Delivery\Controllers\Users', 'update']);
  });
  
  $r->addGroup('/shop', function (\FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', ['Delivery\Controllers\Shop', 'all']);
  });
  

  $r->addGroup('/cart', function (\FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/{id:\d+}', ['Delivery\Controllers\Cart', 'add']);
    $r->addRoute('GET', '/', ['Delivery\Controllers\Cart', 'all']);
    $r->addRoute('DELETE', '/{id:\d+}', ['Delivery\Controllers\Cart', 'delete']);
  });

  $r->addRoute('GET', '/orders', ['Delivery\Controllers\Orders', 'all']);
  $r->addRoute('POST', '/orders', ['Delivery\Controllers\Orders', 'create']);
  $r->addRoute('GET', '/orders/{id:\d+}', ['Delivery\Controllers\Orders', 'show']);
  $r->addRoute('PUT', '/orders/{id:\d+}', ['Delivery\Controllers\Orders', 'update']);

  $r->addRoute('GET', '/', ['Delivery\Controllers\Homepage', 'show']);
  $r->addRoute('GET', '/login', ['Delivery\Controllers\Authentication', 'login_page']);
  $r->addRoute('POST', '/login', ['Delivery\Controllers\Authentication', 'login']);
  $r->addRoute('GET', '/signup', ['Delivery\Controllers\Authentication', 'signup_page']);
  $r->addRoute('POST', '/signup', ['Delivery\Controllers\Authentication', 'signup']);
  $r->addRoute('GET', '/logout', ['Delivery\Controllers\Authentication', 'logout']);
  
};

$dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback);

$method = isset($_POST['_method']) ? $_POST['_method'] : $request->getMethod();

$routeInfo = $dispatcher->dispatch($method, $request->getPath());
switch ($routeInfo[0]) {
  case \FastRoute\Dispatcher::NOT_FOUND:
    $response->setContent('404 - Page not found');
    $response->setStatusCode(404);
    break;
  case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
    $response->setContent('405 - Method not allowed');
    $response->setStatusCode(405);
    break;
  case \FastRoute\Dispatcher::FOUND:
    $className = $routeInfo[1][0];
    $method = $routeInfo[1][1];
    $vars = $routeInfo[2];

    $class = $injector->make($className);
    $class->$method($vars);
    break;
}

foreach ($response->getHeaders() as $header) {
  header($header, false);
}

echo $response->getContent();
