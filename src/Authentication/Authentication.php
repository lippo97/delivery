<?php

namespace Delivery\Authentication;

interface Authentication
{
  public function createUserWithEmailAndPassword(string $email, string $password);
  public function createRestaurantWithEmailAndPassword(string $email,
    string $password, string $restaurantName, string $restaurantTel, string $restaurantAddress);
  public function logInUserWithEmailAndPassword(string $email, string $password);
  public function logOutUser();
  public function isLoggedIn();
  public function isAdmin();
  public function isRestaurant();
  public function getCurrentUserId();
  public function getCurrentUserEmail();
  public function getCurrentUserRestaurantName();
  public function getErrors();
}
