<?php

namespace Delivery\Authentication;

//use Delivery\Authentication\Authentication;
use Http\Request;
use Delivery\Session\SecureSession;

class DatabaseAuthentication implements Authentication
{
  private $request;
  private $errors;

  public function __construct(Request $request = null)
  {
    $this->request = $request;
  }

  public function createUserWithEmailAndPassword(string $email, string $password)
  {
    return $this->createGenericUserWithEmailAndPassword($email, $password);
  }

  public function createRestaurantWithEmailAndPassword(string $email, string $password,
    string $restaurantName, string $restaurantTel, string $restaurantAddress)
  {
    return $this->createGenericUserWithEmailAndPassword($email, $password, function($user) use ($restaurantName, $restaurantTel, $restaurantAddress) {
      $user->setRestaurant(true);
      $user->setRestaurantName($restaurantName);
      $user->setRestaurantTel($restaurantTel);
      $user->setRestaurantAddress($restaurantAddress);
    });
  }

  private function createGenericUserWithEmailAndPassword(string $email, string $password, $userModify = null)
  {
    $this->clearErrors();
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    $password = hash('sha512', $password.$random_salt);
    $user = new \User();
    $user->setEmail($email);
    $user->setPassword($password);
    $user->setSalt($random_salt);

    if ($userModify != null) {
      $userModify($user);
    }

    if (!$user->validate()) {
      // deal with errors
      // TODO
      $this->errors = [ "Errori nei dati forniti" ];
      return false;
    } else {
      try {
        $user->save();

        $user_browser = $this->request->getUserAgent();
        $user_id = $user->getId();
        $user_email = $user->getEmail();
        $administrator = $user->getAdministrator();
        $restaurant = $user->getRestaurant();
        $login_string = hash('sha512', $user_browser);
        $restaurant_name = $user->getRestaurantName();

        $session = SecureSession::getIstance();
        $session->set('user_id', $user_id);
        $session->set('user_email', $user_email);
        $session->set('administrator', $administrator);
        $session->set('login_string', $login_string);
        $session->set('restaurant',$restaurant);
        if ($restaurant_name != null) {
          $session->set('user_restaurant_name', $restaurant_name);
        }
        return true;
      } catch (\Propel\Runtime\Exception\PropelException $e) {
        $this->errors = [ "Questa email è già utilizzata." ];
        return false;
      }
      //$this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function logInUserWithEmailAndPassword(string $email, string $password)
  {
    $q = new \UserQuery();
    $user = $q->findOneByEmail($email);
    if ($user !== null) {
      $db_password = $user->getPassword();
      $salt = $user->getSalt();
      $salted_password = hash('sha512', $password.$salt);
      // If password matches
      if ($db_password == $salted_password) {
        $user_browser = $this->request->getUserAgent();
        $user_id = $user->getId();
        $user_email = $user->getEmail();
        $administrator = $user->getAdministrator();
        $restaurant = $user->getRestaurant();
        $restaurant_name = $user->getRestaurantName();
        $login_string = hash('sha512', $user_browser);

        // Set session and return true
        $session = SecureSession::getIstance();
        $session->set('user_id', $user_id);
        $session->set('user_email', $user_email);
        $session->set('administrator', $administrator);
        $session->set('login_string', $login_string);
        $session->set('restaurant',$restaurant);
        $session->set('user_restaurant_name', $restaurant_name);
        return true;
      } else {
        // Wrong password
        $this->errors = [ "Password errata." ];
        return false;
      }
    } else {
      // Wrong email
      $this->errors = [ "L'email fornita non corrisponde a nessun utente." ];
      return false;
    }
  }

  public function logOutUser()
  {
    SecureSession::getIstance()->destroy();
  }

  public function isLoggedIn()
  {
    $session = SecureSession::getIstance();
    if ($session->has('login_string')) {
      $login_string = $session->get('login_string');
      return hash('sha512', $this->request->getUserAgent()) == $login_string;
    }
    return false;
  }

  public function isAdmin()
  {
    return SecureSession::getIstance()->get('administrator');
  }

  public function isRestaurant()
  {
    return SecureSession::getIstance()->get('restaurant');
  }
  
  public function getCurrentUserId()
  {
    return SecureSession::getIstance()->get('user_id');
  }
  
  public function getCurrentUserEmail()
  {
    return SecureSession::getIstance()->get('user_email');
  }

  public function getCurrentUserRestaurantName()
  {
    return SecureSession::getIstance()->get('user_restaurant_name');
  }

  public function getErrors()
  {
    return $this->errors;
  }

  private function clearErrors()
  {
    $this->errors = array();
  }
}
