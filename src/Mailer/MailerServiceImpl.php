<?php

namespace Delivery\Mailer;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailerServiceImpl implements MailerService
{
  private $mail;

  public function __construct()
  {
    $this->mail = new PHPMailer(true);
    $this->mail->SMTPDebug = 1;
    $this->mail->isSMTP();
    $this->mail->Host = 'smtp.gmail.com';
    $this->mail->SMTPAuth = true;
    $this->mail->Username = 'deliverymail73@gmail.com';
    $this->mail->Password = 'PolveSbriciolone1';
    $this->mail->SMTPSecure = 'tls';
    $this->mail->Port = 587;

    $this->mail->setFrom('deliverymail73@gmail.com', 'Delivery AMS');
    $this->mail->isHTML(true);
  }

  public function welcomeUser($user)
  {
    $email = $user['email'];

    $this->mail->addAddress($email);
    $this->mail->Subject = "Delivery: Benvenuto $email, inizia subito a ordinare!";
    $this->mail->Body = "Gentile utente, questa e' un'email automatica per notificare l'avvenuta registrazione al servizio. D'ora in avanti e' libero di effettuare ordini.";
    $this->mail->AltBody = "Gentile utente, questa e' un'email automatica per notificare l'avvenuta registrazione al servizio. D'ora in avanti e' libero di effettuare ordini.";
    try {
      $this->mail->send();
    } catch (Exception $e) {
    }
  }

  public function welcomeRestaurant($user)
  {
    $restaurantName = $user['restaurantName'];
    $email = $user['email'];

    $this->mail->addAddress($email);
    $this->mail->Subject = "Delivery: Benvenuto $restaurantName, sarai presto abilitato al servizio.";
    $this->mail->Body = "Gentile cliente, questa e' un'email automatica per notificarla di aver preso visione della sua richiesta di abilitazione. Ricevera' presto aggiornamenti a riguardo.";
    $this->mail->AltBody = "Gentile cliente, questa e' un'email automatica per notificarla di aver preso visione della sua richiesta di abilitazione. Ricevera' presto aggiornamenti a riguardo.";
    try {
      $this->mail->send();
    } catch (Exception $e) {
    }
  }

  public function notifyRestaurantEnabled($user) 
  {
    $email = $user['email'];

    $this->mail->addAddress($email);
    $this->mail->Subject = "Delivery: l'abilitazione del suo ristorante e' stata aggiornata.";
    $this->mail->Body = "Gentile cliente, questa e' un'email automatica per aggiornarla dell'aggiornamento dello stato di abilitazione del suo ristorante. Si e' pregati di non rispondere a questo indirizzo.";
    $this->mail->AltBody = "Gentile cliente, questa e' un'email automatica per aggiornarla dell'aggiornamento dello stato di abilitazione del suo ristorante. Si e' pregati di non rispondere a questo indirizzo.";
    try {
      $this->mail->send();
    } catch (Exception $e) {
    }
  }
  
  public function notifyUserOfOrder($user, $order) {
    $email = $user['email'];
    $processed = $order['processed'];
    $time = date_format($order['deliveryTime'], "H:i");

    $this->mail->addAddress($email);
    if (!$processed) {
      $this->mail->Subject = "Delivery: il tuo ordine e stato ricevuto.";
      $this->mail->Body = "Ciao, $email. Questa e' un'email di notifica della ricevuta del tuo ordine.";
      $this->mail->AltBody = "Ciao, $email. Questa e' un'email di notifica della ricevuta del tuo ordine.";
    } else {
      $this->mail->Subject = "Delivery: il tuo ordine e stato evaso.";
      $this->mail->Body = "Ciao, $email. Il tuo ordine e' stato evaso, la consegna avverra' alle ore $time.";
      $this->mail->AltBody = "Ciao, $email. Il tuo ordine e' stato evaso, la consegna avverra' alle ore $time.";
    }
    try {
      $this->mail->send();
    } catch (Exception $e) {
    }
  }

  public function notifyRestaurantOfOrder($user, $order) {
    $email = $user['email'];
    $time = date_format($order['deliveryTime'], "H:i");

    $mail = new PHPMailer(true);
    $mail->SMTPDebug = 1;
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'deliverymail73@gmail.com';
    $mail->Password = 'PolveSbriciolone1';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->setFrom('deliverymail73@gmail.com', 'Delivery AMS');
    $mail->isHTML(true);

    $mail->addAddress($email);
    $mail->Subject = "Delivery: nuovo ordine per il suo ristorante";
    $mail->Body = "Gentile cliente, questa e' un'email automatica per notificarla di una nuova ordinazione per il suo ristorante per le ore $time.";
    $mail->AltBody = "Gentile cliente, questa e' un'email automatica per notificarla di una nuova ordinazione per il suo ristorante per le ore $time.";

    try {
      $mail->send();
    } catch (Exception $e) {
    }
  }
}
