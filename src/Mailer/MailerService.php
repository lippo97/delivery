<?php

namespace Delivery\Mailer;

interface MailerService
{
  public function welcomeUser($user);
  public function welcomeRestaurant($user);
  public function notifyRestaurantEnabled($user);
  public function notifyUserOfOrder($user, $order);
  public function notifyRestaurantOfOrder($user, $order);
        // $this->mailerService->notifyUserOfOrder($order->getClient(), $order);
        // $this->mailerService->notifyRestaurantOfOrder($order->getSeller(), $order);
}
