<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;

class Homepage
{
  private $request;
  private $reponse;
  private $renderer;

  public function __construct(Request $request, Response $response, Renderer $renderer)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
  }

  public function show()
  {
    $html = $this->renderer->render('Homepage');
    $this->response->setContent($html);
  }
}
