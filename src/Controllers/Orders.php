<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\HttpCookie;
use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;
use Delivery\Mailer\MailerService;

class Orders
{
  private $request;
  private $reponse;
  private $renderer;
  private $authentication;
  private $mailerService;
  private $base_url = "/categories/";
  private $user_access;

  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication, MailerService $mailerService)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;
    $this->mailerService = $mailerService;
    $this->user_access = "" . $this->authentication->getCurrentUserId();
  }

  public function all()
  {
    if ($this->authentication->isLoggedIn() && $this->authentication->isRestaurant()) {
      $processed = $this->request->getParameter('processed', false);
      $page = $this->request->getParameter('page', 1);
      $perPage = 10;
     /* $cookie = $_COOKIE[$this->user_access];
      $cookie = json_decode($cookie,true);
      $value = json_encode($cookie);
      setcookie($this->user_access, $value, time()+86400);// 1 giorno*/
      
      $pagination = \RestaurantOrderQuery::create()
        ->filterBySellerId($this->authentication->getCurrentUserId())
        ->filterByProcessed($processed)
        ->orderByDeliveryTime()
        ->paginate($page, $perPage);
        
      $html = $this->renderer->render('orders/all', [
        "orders" => $pagination,
        "processed" => $processed,
        'pages' => $pagination->getLinks(5),
        'currentPage' => $page
      ]);
      $this->response->setContent($html);
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function show($params)
  {
    if ($this->authentication->isLoggedIn() && isset($params['id'])) {
      $order = \RestaurantOrderQuery::create()
        ->findPK($params['id']);
      if ($this->authentication->getCurrentUserId() == $order->getSellerId()) {
        $html = $this->renderer->render('orders/show', [
          "order" => $order
        ]);
        return $this->response->setContent($html);
      }
    }
    $this->response->redirect($GLOBALS['ROOT']);
  }

  public function create()
  {
    if ($this->authentication->isLoggedIn()) {
      $deliveryPoint = $this->request->getParameter('delivery_point', null);
      $time = $this->request->getParameter('time', null);
      if (isset($deliveryPoint) && isset($time)) {
        //$cookies = $_COOKIE[$this->user_access];
        $cookieValue = $this->request->getCookie($this->user_access);
        $cookies = json_decode($cookieValue,true);
        setcookie($this->user_access, "", -1 ,"/");
       // $cookieIstance = new HttpCookie($this->user_access,"");
       // $this->response->deleteCookie($cookieIstance);
        $data = [];
        foreach ($cookies as $id => $qty) {
          $idSeller = \ArticleQuery::create()->filterById($id)
                                             ->findOne()
                                             ->getUserId();
          if (!isset($data[$idSeller])) {
            $data[$idSeller] = [];
          }
          array_push($data[$idSeller], [ $id, $qty]);
        }

        foreach ($data as $idSeller => $articles) {
          $order = new \RestaurantOrder();
          $order->setClientId($this->authentication->getCurrentUserId());
          $order->setSellerId($idSeller);
          $order->setDeliveryTime($time);
          $order->setDeliveryPointId($deliveryPoint);
          $order->save();

          //$this->mailerService->notifyUserOfOrder([
            //'email' => $order->getClient()->getEmail()
          //], [
            //'processed' => $order->getProcessed(),
            //'deliveryTime' => $order->getDeliveryTime()
          //]);



          foreach ($articles as $article) {
            $articleId = $article[0];
            $qty = $article[1];
            $orderItem = new \OrderItem();
            $orderItem->setRestaurantOrder($order);
            $orderItem->setArticleId($articleId);
            $orderItem->setWeight($qty);
            try {
              $orderItem->save();
            } catch (\Propel\Runtime\PropelException\Exception $e) {
              $e->getMessage();
            }
          }
        }
        $this->mailerService->notifyRestaurantOfOrder([
          'email' => $order->getSeller()->getEmail()
        ], [
          'deliveryTime' => $order->getDeliveryTime()
        ]);
        $this->response->redirect($GLOBALS['ROOT'] . '/cart/?orderSuccess=1');
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function update($params)
  {
    if ($this->authentication->isLoggedIn() && isset($params['id'])) {
      $order = \RestaurantOrderQuery::create()
        ->findPK($params['id']);
      if ($this->authentication->getCurrentUserId() == $order->getSellerId()) {
        if (!$order->getProcessed()) {
          $order->setProcessed(true);
          $order->save();
          $this->mailerService->notifyUserOfOrder([
            'email' => $order->getClient()->getEmail()
          ], [
            'processed' => $order->getProcessed(),
            'deliveryTime' => $order->getDeliveryTime()
          ]);
          return $this->response->redirect($GLOBALS['ROOT'] . '/orders/' . $order->getId());
        }
      }
    }
    $this->response->redirect($GLOBALS['ROOT']);
  }

}
