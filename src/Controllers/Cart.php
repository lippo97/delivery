<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\HttpCookie;
use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;


class Cart
{
  private $request;
  private $reponse;
  private $renderer;
  private $base_url = "/articles/";
  private $user_access;

  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
	$this->authentication = $authentication;
	$this->user_access = "" . $this->authentication->getCurrentUserId();


  }

  public function all()
  {
    
    if ($this->authentication->isLoggedIn()) {
		//$params['user_email'] = $databaseAuthentication->getCurrentUserEmail();
    $orderSuccess = $this->request->getParameter('orderSuccess', false);
		$cookieIstance = $this->request->getCookie($this->user_access);
	  if($cookieIstance != null){
			
		/*	$cookie = $_COOKIE[$this->user_access];*/
			$cookie = json_decode($cookieIstance,true);
			
			$q = new \ArticleQuery();
			$qP = new \DeliveryPointQuery();
			$params = array(
				'cookie' => $cookie,
				'articles' => $q->find(),
				'points' => $qP->find(),
				'orderSuccess' => $orderSuccess
			);
			$html = $this->renderer->render('cart/all' , $params);

			$this->response->setContent($html);
	   
	} else{
    $html = $this->renderer->render('cart/all', [
      'orderSuccess' => $orderSuccess
    ]);
  
		$this->response->setContent($html);
	  }
	} else {
		$this->response->redirect($GLOBALS['ROOT'] . '/login');
	}
}
	 
  

  
  public function add($params)
  {
    
    if ($this->authentication->isLoggedIn()) {
		//$params['user_email'] = $databaseAuthentication->getCurrentUserEmail();
	  if(isset($params['id'])){
		
	 	$cookieIstance = $this->request->getCookie($this->user_access);
		if($cookieIstance == null){
			$value = array($params['id'] => 1);
			$value = json_encode($value);
			$cookieIstance = new HttpCookie($this->user_access,$value);
			$cookieIstance->setMaxAge(time() + 86400);
			$cookieIstance->setPath("/");
			$this->response->addCookie($cookieIstance);
		}
		else{
		//	$cookie = $_COOKIE[$this->user_access];
			
			$cookie = json_decode($cookieIstance,true);
			
			
			if(array_key_exists($params['id'],$cookie)){
				$cookie[$params['id']] =  $cookie[$params['id']]+1;
			}
			else{
				$cookie[$params['id']] = 1;
				
			}
			$value = json_encode($cookie);
			$cookieIstance = new HttpCookie($this->user_access,$value);
			$cookieIstance->setMaxAge(time() + 86400);
			$cookieIstance->setPath("/");

			$this->response->addCookie($cookieIstance);// 1 giorno

		
		}
		
		 $q = new \ArticleQuery();
		 $article = $q->findPk($params['id']);
		 $this->response->redirect($GLOBALS['ROOT'] . '/users/'.$article->getUserId() );
	  }
	 		

     } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
    //var_dump($this->response);
	}
	
  public function delete($params)
  {
	if ($this->authentication->isLoggedIn() && !$this->authentication->isRestaurant()  ) {
				//$cookie = $_COOKIE[$this->user_access];
				$cookieIstance = $this->request->getCookie($this->user_access);
				$cookie = json_decode($cookieIstance,true);
				if(isset($params['id'])){
					if(isset($_COOKIE[$this->user_access]) && array_key_exists($params['id'],$cookie)){
						if($cookie[$params['id']] > 1){
							$cookie[$params['id']] =  $cookie[$params['id']]-1;
						}
						else{
							unset($cookie[$params['id']]);

						}
						$value = json_encode($cookie);
            $cookieIstance = new HttpCookie($this->user_access,$value);
						$cookieIstance->setMaxAge(time() + 86400);
						$cookieIstance->setPath("/");

						$this->response->addCookie($cookieIstance);//1 giorno
					}
					 
					$q = new \ArticleQuery();
						 //$html = $this->renderer->render('cart/all', array(
						//'cookie' => $cookie,
						//'articles' => $q->find()
						//));
						//$this->response->setContent($html);
          $this->response->redirect($GLOBALS['ROOT'] . '/cart/');
				
			 
		} else {
			$this->response->redirect($GLOBALS['ROOT']);

		} 
	} else {
		$this->response->redirect($GLOBALS['ROOT'] . '/login');
	}
  }
}
