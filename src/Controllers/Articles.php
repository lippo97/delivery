<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;


class Articles
{
  private $request;
  private $reponse;
  private $renderer;
  private $base_url;

  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;
    $this->base_url = '/users/';
  }

  public function all()
  {
    
    if ($this->authentication->isLoggedIn()) {
      //$params['user_email'] = $databaseAuthentication->getCurrentUserEmail();
		if ($this->authentication->isRestaurant()) {
			$q = new \ArticleQuery();
			$params = array(
				'articles' => $q->filterByUserId($this->authentication->getCurrentUserId())
			);
			$html = $this->renderer->render('articles/all', $params);

			$this->response->setContent($html);

		} else {
			$this->response->redirect($GLOBALS['ROOT']);

		}
     } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
    //var_dump($this->response);
  }

  

  public function insert()
  {
	  if ($this->authentication->isLoggedIn()) {
			if ($this->authentication->isRestaurant()) {
				$q = new \CategoryQuery();
				$params = array(
				'categories' => $q->find()
				);
				$html = $this->renderer->render('articles/insert',$params);

				$this->response->setContent($html);
			} else {
				$this->response->redirect($GLOBALS['ROOT']);

			}
	  } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function create()
  {
	if ($this->authentication->isLoggedIn()) {
		if ($this->authentication->isRestaurant()) {
			$name = $this->request->getParameter('name');
			$category = $this->request->getParameter('category');
			$price = $this->request->getParameter('price');

			$article = new \Article();
			$article->setName($name);
			$article->setUserId($this->authentication->getCurrentUserId());
			$article->setCategoryId($category);
			$article->setPrice($price);


			if (!$article->validate()) {
				$errors = [
          "Dati inseriti non validi"
				];
				$html = $this->renderer->render("articles/insert", array(
          'errors' => $errors,
          'categories' => \CategoryQuery::create()->find()
        ));
        $this->response->setContent($html);
			} else {
				$article->save();
        if (!empty($_FILES['image']['name'])) {
          $extsAllowed = array( 'jpg', 'jpeg', 'png' );
          $extUpload = strtolower( substr( strrchr($_FILES['image']['name'], '.') ,1) ) ;
          if (in_array($extUpload, $extsAllowed)) {
            $name = "/uploads/articles/{$article->getId()}.$extUpload";
            $article->setImagePath($name);
            $article->save();
            move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT . $name);
          } else {
            array_push($errors, "Le estensioni consentite per l'immagine sono jpg, jpeg, png");
            $html = $this->renderer->render("articles/insert", array(
              'errors' => $errors
            ));
            return $this->response->setContent($html);
          }
        }
				$this->response->redirect($GLOBALS['ROOT'] . $this->base_url . $this->authentication->getCurrentUserId());
			}
		} else {
			$this->response->redirect($GLOBALS['ROOT']);
		}
	 } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function edit($params)
  {
	if ($this->authentication->isLoggedIn()) {
		if ($this->authentication->isRestaurant()) {
			if (isset($params['id'])) {
				$qC = new \CategoryQuery();

				$id = $params['id'];
				$q = new \ArticleQuery();
				$article = $q->findPK($id);
				if ($article !== NULL) {
					$html = $this->renderer->render('articles/edit', array(
					'article' => $article,
					'categories' => $qC->find()
					));
					$this->response->setContent($html);
				} else {
					$this->response->redirect($GLOBALS['ROOT'] . $this->base_url . $this->authentication->getCurrentUserId());
				}
			}
		} else {
			$this->response->redirect($GLOBALS['ROOT']);
		}
	 } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function update($params)
  {
	if ($this->authentication->isLoggedIn()) {
		if ($this->authentication->isRestaurant()) {
			if (isset($params['id'])) {
				$id = $params['id'];
				$name = $this->request->getParameter('name', '');
				$category = $this->request->getParameter('category');
				$price = $this->request->getParameter('price','');
				$q = new \ArticleQuery();
				$article = $q->findPK($id);
				$article->setName($name);
				$article->setCategoryId($category);
				$article->setPrice($price);
				if (!$article->validate()) {
					$errors = [
					"errore1",
					"errore2",
					"errore3"
					];
					$html = $this->renderer->render("articles/edit", array(
					'errors' => $errors,
					'article' => $article
					));
					$this->response->setContent($html);
				} else {
					$article->save();
          if (!empty($_FILES['image']['name'])) {
            $extsAllowed = array( 'jpg', 'jpeg', 'png' );
            $extUpload = strtolower( substr( strrchr($_FILES['image']['name'], '.') ,1) ) ;
            if (in_array($extUpload, $extsAllowed)) {
              $name = "/uploads/articles/{$article->getId()}.$extUpload";
              $article->setImagePath($name);
              $article->save();
              move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT . $name);
            } else {
              array_push($errors, "Le estensioni consentite per l'immagine sono jpg, jpeg, png");
              $html = $this->renderer->render("articles/insert", array(
                'errors' => $errors
              ));
              return $this->response->setContent($html);
            }
          }
					$this->response->redirect($GLOBALS['ROOT'] . $this->base_url . $this->authentication->getCurrentUserId());
				}
			}
		} else {
		$this->response->redirect($GLOBALS['ROOT']);

		}
	 } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function delete($params)
  {
	if ($this->authentication->isLoggedIn()) {
		if ($this->authentication->isRestaurant()) {
			if (isset($params['id'])) {
				$id = $params['id'];
				$q = new \ArticleQuery();
				$article = $q->findPK($id);
				try {
					$article->delete();
					$this->response->redirect($GLOBALS['ROOT'] . $this->base_url . $this->authentication->getCurrentUserId());
				} catch (\Propel\Runtime\Exception\PropelException $e) {
					$errors = [
            "Non e' stato possibile rimuovere l' articolo selezionato."
					];
					$html = $this->renderer->render('articles/all', array(
					'errors' => $errors,
					'articles' => \ArticleQuery::create()->findByUserId($this->authentication->getCurrentUserId())
					));
					$this->response->setContent($html);
				}
			} 
		} else {
			$this->response->redirect($GLOBALS['ROOT']);

		} 
	} else {
		$this->response->redirect($GLOBALS['ROOT'] . '/login');
	}
  }
}
