<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;


class Shop
{
  private $request;
  private $reponse;
  private $renderer;
  private $base_url = "/shop/";

  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;

  }

  public function all()
  {
    
    if ($this->authentication->isLoggedIn()) {
      $food=$this->request->getParameter('food',"");
      $category=$this->request->getParameter('category',"");
      $restaurant=$this->request->getParameter('restaurant',"");
      
      //var_dump($this->request);
      //$params['user_email'] = $databaseAuthentication->getCurrentUserEmail();
      $qR = new \UserQuery();
      $qR->filterByRestaurant(true)
         ->filterByRestaurantEnabled(true);
         
      $qC = new \CategoryQuery();
      if(!empty($food)){
        $qR->useArticleQuery()
          ->where('INSTR(article.name,?)',$food)
          ->endUse();
          
          
      }
      if(!empty($category)){
        $qR->useArticleQuery()
        ->filterByCategoryId($category)
        ->endUse();
      }
      if(!empty($restaurant)){
          $qR->where('INSTR(user.restaurant_name,?)',$restaurant);
       }



      $html = $this->renderer->render('shop/all', array(
    
        'shops' => $qR->find(),
        'categories' => $qC->find()
        
        ));
			

			$this->response->setContent($html);
     } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
    //var_dump($this->response);
  }

}
