<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;

class Categories
{
  private $request;
  private $reponse;
  private $renderer;
  private $authentication;
  private $base_url = "/categories/";

  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;
  }

  public function all()
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        $page = $this->request->getParameter('page', 1);
        $perPage = 10;
        $q = new \CategoryQuery();
        $pagination = $q->paginate($page, $perPage);
        $html = $this->renderer->render('categories/all', array(
          'categories' => $pagination,
          'pages' => $pagination->getLinks(5),
          'currentPage' => $page
        ));
        $this->response->setContent($html);
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function insert()
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        $html = $this->renderer->render('categories/insert');
        $this->response->setContent($html);
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function create()
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        $name = $this->request->getParameter('name');
        $category = new \Category();
        $category->setName($name);
        if (!$category->validate()) {
          foreach ($category->getValidationFailures() as $failure) {
            echo "Property ".$failure->getPropertyPath().": ".$failure->getMessage()."\n";
          }
          $errors = [
            "errore1",
            "errore2",
            "errore3"
          ];
          $html = $this->renderer->render("categories/insert", array(
            'errors' => $errors
          ));
          $this->response->setContent($html);
        } else {
          $category->save();
          $this->response->redirect($GLOBALS['ROOT'] . $this->base_url);
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }

  }

  public function edit($params)
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        if (isset($params['id'])) {
          $id = $params['id'];
          $q = new \CategoryQuery();
          $category = $q->findPK($id);
          if ($category !== NULL) {
            $html = $this->renderer->render('categories/edit', array(
              'category' => $category
            ));
            $this->response->setContent($html);
          } else {
            $this->response->redirect($GLOBALS['ROOT'] . $this->base_url);
          }
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }

  }

  public function update($params)
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        if (isset($params['id'])) {
          $id = $params['id'];
          $name = $this->request->getParameter('name', '');
          $enabled = $this->request->getParameter('enabled', false);
          $q = new \CategoryQuery();
          $category = $q->findPK($id);
          $category->setName($name);
          $category->setEnabled($enabled);
          if (!$category->validate()) {
            foreach ($category->getValidationFailures() as $failure) {
              //echo "Property ".$failure->getPropertyPath().": ".$failure->getMessage()."\n";
              //var_dump($failure);
            }
            $errors = [
              "L'elenco degli errori potrebbe prevedere testi lunghi cosi.",
              "La lunghezza del testo non dovrebbe sballare il layout.",
              "errore3"
            ];
            $html = $this->renderer->render("categories/edit", array(
              'errors' => $errors,
              'category' => $category
            ));
            $this->response->setContent($html);
          } else {
            $category->save();
            $this->response->redirect($GLOBALS['ROOT'] . $this->base_url);
          }
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }

  }

  public function delete($params)
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        if (isset($params['id'])) {
          $id = $params['id'];
          $q = new \CategoryQuery();
          $category = $q->findPK($id);
          try {
            $category->delete();
            $this->response->redirect($GLOBALS['ROOT']. $this->base_url);
          } catch (\Propel\Runtime\Exception\PropelException $e) {
            $categories = $q->find();
            $errors = [
              "Non e' stato possibile rimuovere la categoria selezionata. Esistono articoli sotto questa categoria."
            ];
            $html = $this->renderer->render('categories/all', array(
              'errors' => $errors,
              'categories' => $categories
            ));
            $this->response->setContent($html);
          }
        }  
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }
}
