<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;

class DeliveryPoints
{
  private $request;
  private $reponse;
  private $renderer;
  private $authentication;
  private $base_url = "/delivery_points/";

  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;
  }

  public function all()
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        $page = $this->request->getParameter('page', 1);
        $perPage = 10;
        $q = new \DeliveryPointQuery();
        $pagination = $q->paginate($page, $perPage);
        $html = $this->renderer->render('deliveryPoints/all', array(
          'deliveryPoints' => $pagination,
          'pages' => $pagination->getLinks(5),
          'currentPage' => $page
        ));
        $this->response->setContent($html);
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function insert()
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        $html = $this->renderer->render('deliveryPoints/insert');
        $this->response->setContent($html);
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function create()
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        $name = $this->request->getParameter('name');
        $deliveryPoint = new \DeliveryPoint();
        $deliveryPoint->setName($name);
        if (!$deliveryPoint->validate()) {
          foreach ($deliveryPoint->getValidationFailures() as $failure) {
            echo "Property ".$failure->getPropertyPath().": ".$failure->getMessage()."\n";
          }
          $errors = [
            "errore1",
            "errore2",
            "errore3"
          ];
          $html = $this->renderer->render("deliveryPoints/insert", array(
            'errors' => $errors
          ));
          $this->response->setContent($html);
        } else {
          $deliveryPoint->save();
          $this->response->redirect($GLOBALS['ROOT'] . $this->base_url);
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }

  }

  public function edit($params)
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        if (isset($params['id'])) {
          $id = $params['id'];
          $q = new \DeliveryPointQuery();
          $deliveryPoint = $q->findPK($id);
          if ($deliveryPoint !== NULL) {
            $html = $this->renderer->render('deliveryPoints/edit', array(
              'deliveryPoint' => $deliveryPoint
            ));
            $this->response->setContent($html);
          } else {
            $this->response->redirect($GLOBALS['ROOT'] . $this->base_url);
          }
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }

  }

  public function update($params)
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        if (isset($params['id'])) {
          $id = $params['id'];
          $name = $this->request->getParameter('name', '');
          $enabled = $this->request->getParameter('enabled', false);
          $q = new \DeliveryPointQuery();
          $deliveryPoint = $q->findPK($id);
          $deliveryPoint->setName($name);
          $deliveryPoint->setEnabled($enabled);
          if (!$deliveryPoint->validate()) {
            foreach ($deliveryPoint->getValidationFailures() as $failure) {
              //echo "Property ".$failure->getPropertyPath().": ".$failure->getMessage()."\n";
              //var_dump($failure);
            }
            $errors = [
              "L'elenco degli errori potrebbe prevedere testi lunghi cosi.",
              "La lunghezza del testo non dovrebbe sballare il layout.",
              "errore3"
            ];
            $html = $this->renderer->render("deliveryPoints/edit", array(
              'errors' => $errors,
              'deliveryPoint' => $deliveryPoint
            ));
            $this->response->setContent($html);
          } else {
            $deliveryPoint->save();
            $this->response->redirect($GLOBALS['ROOT'] . $this->base_url);
          }
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }

  }

  public function delete($params)
  {
    if ($this->authentication->isLoggedIn()) {
      if ($this->authentication->isAdmin()) {
        if (isset($params['id'])) {
          $id = $params['id'];
          $q = new \DeliveryPointQuery();
          $deliveryPoint = $q->findPK($id);
          try {
            $deliveryPoint->delete();
            $this->response->redirect($GLOBALS['ROOT']. $this->base_url);
          } catch (\Propel\Runtime\Exception\PropelException $e) {
            $deliveryPoints = $q->find();
            $errors = [
              "Non e' stato possibile rimuovere la categoria selezionata. Esistono articoli sotto questa categoria."
            ];
            $html = $this->renderer->render('deliveryPoints/all', array(
              'errors' => $errors,
              'deliveryPoints' => $deliveryPoints
            ));
            $this->response->setContent($html);
          }
        }  
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }
}
