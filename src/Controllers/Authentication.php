<?php declare(strict_types = 1);

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication as AuthenticationService;
use Delivery\Authentication\DatabaseAuthentication;
use Delivery\Mailer\MailerService;

class Authentication
{
  private $request;
  private $response;
  private $renderer;
  private $authentication;
  private $mailerService;

  public function __construct(Request $request, Response $response, Renderer $renderer, AuthenticationService $authentication, MailerService $mailerService)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;
    $this->mailerService = $mailerService;
  }

  public function signup_page()
  {
    if (!$this->authentication->isLoggedIn()) {
      $html = $this->renderer->render('authentication/signup');
      $this->response->setContent($html);
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function signup()
  {
    if (!$this->authentication->isLoggedIn()) {
      $email = $this->request->getParameter('email');
      $password = $this->request->getParameter('password');
      $password_confirmation = $this->request->getParameter('password_confirmation');
      $is_restaurant = $this->request->getParameter('is_restaurant');
      $restaurant_name = $this->request->getParameter('restaurant_name');
      $restaurant_tel = $this->request->getParameter('restaurant_tel');
      $restaurant_address = $this->request->getParameter('restaurant_address');

      if (!(empty($email) || empty($password) || empty($password_confirmation)) &&
        ($is_restaurant == (!empty($restaurant_name) && !empty($restaurant_tel) && !empty($restaurant_address)))
      ) {
        if ($password === $password_confirmation) {
          $databaseAuthentication = new \Delivery\Authentication\DatabaseAuthentication($this->request);
          if ($is_restaurant) {
            $res = $databaseAuthentication->createRestaurantWithEmailAndPassword($email, $password,
              $restaurant_name, $restaurant_tel, $restaurant_address);
          } else {
            $res = $databaseAuthentication->createUserWithEmailAndPassword($email, $password);
          }
          if ($res) {
            if ($is_restaurant) {
              $this->mailerService->welcomeRestaurant(array(
                'restaurantName' => $restaurant_name,
                'email' => $email
              ));
            } else {
              $this->mailerService->welcomeUser(array(
                'email' => $email
              ));
            }
            $this->response->redirect($GLOBALS['ROOT']);
          } else {
            $html = $this->renderer->render('authentication/signup', array(
              'errors' => $databaseAuthentication->getErrors()
            ));
            $this->response->setContent($html);
          }
        } else {
          $html = $this->renderer->render('authentication/signup', array(
            'errors' => [ "Le due password non coincidono." ]
          ));
          $this->response->setContent($html);
        }
      } else {
        $html = $this->renderer->render('authentication/signup', array(
          'errors' => [ "Tutti i campi devono essere compilati." ]
        ));
        $this->response->setContent($html);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function login_page()
  {
    if (!$this->authentication->isLoggedIn()) {
    $html = $this->renderer->render('authentication/login');
    $this->response->setContent($html);
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function login()
  {
    if (!$this->authentication->isLoggedIn()) {
      $email = $this->request->getParameter('email');
      $password = $this->request->getParameter('password');
      $databaseAuthentication = new DatabaseAuthentication($this->request);
      if($databaseAuthentication->logInUserWithEmailAndPassword($email , $password)) {
        if ($this->authentication->isAdmin()) {
          $this->response->redirect($GLOBALS['ROOT'] . '/categories/');
        } else {
          $this->response->redirect($GLOBALS['ROOT']);
        }
      } else {
        $html = $this->renderer->render('authentication/login', array(
          'errors' => $databaseAuthentication->getErrors()
        ));
        $this->response->setContent($html);
      }
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function logout()
  {
    $databaseAuthentication = new DatabaseAuthentication($this->request);
    if ($databaseAuthentication->isLoggedIn()) {
      $databaseAuthentication->logOutUser();
    }
    $this->response->redirect($GLOBALS['ROOT']);
  }
}
