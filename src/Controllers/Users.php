<?php

namespace Delivery\Controllers;

use Http\Request;
use Http\Response;
use Delivery\Template\Renderer;
use Delivery\Authentication\Authentication;
use Delivery\Mailer\MailerService;

/**
 * Class Users
 * @author yourname
 */
class Users
{
  public function __construct(Request $request, Response $response, Renderer $renderer, Authentication $authentication, MailerService $mailerService)
  {
    $this->request = $request;
    $this->response = $response;
    $this->renderer = $renderer;
    $this->authentication = $authentication;
    $this->mailerService = $mailerService;
  }

  public function all()
  {
    if ($this->authentication->isLoggedIn() && $this->authentication->isAdmin()) {
      $page = $this->request->getParameter('page', 1);
      $perPage = 10;
      $q = new \UserQuery();
      $pagination = $q
        ->filterByRestaurant(true)
        ->paginate($page, $perPage);
      $html = $this->renderer->render('admin/users/all', array(
        'users' => $pagination,
        'pages' => $pagination->getLinks(5),
        'currentPage' => $page
      ));
      $this->response->setContent($html);
    } else {
      $this->response->redirect($GLOBALS['ROOT'] . '/login');
    }
  }

  public function show($params)
  {
    $id = $params['id'];
    if (isset($id)) {
      $user = \UserQuery::create()->findOneById($id);
      if ($user !== null) {
        if ($user->getRestaurant()) {
          if ($user->getId() == $this->authentication->getCurrentUserId()) {
            $html = $this->renderer->render('articles/all', [
              'articles' => $user->getArticles()
            ]);
          } else if ($user->getRestaurantEnabled()) {
            $html = $this->renderer->render('restaurant/show', [
              'user' => $user,
              //'articles' => $user->getArticles()->orderByUserId()
              'articles' => \ArticleQuery::create()->filterByUserId($user->getId())
                                                   ->orderByCategoryId()
                                                   ->find()
            ]);
          } else {
            $this->response->redirect($GLOBALS['ROOT']);
          }
          $this->response->setContent($html);
        } else {
          $this->response->redirect($GLOBALS['ROOT']);
        }
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    }
  }

  public function edit($params)
  {
    if ($this->authentication->isLoggedIn() && $this->authentication->isAdmin() && isset($params['id'])) {
      $q = new \UserQuery();
      $user = $q->findPK($params['id']);
      if ($user !== NULL && $user->getRestaurant()) {
        $html = $this->renderer->render('admin/users/edit', array(
          'user' => $user
        ));
        $this->response->setContent($html);
      } else {
        $this->response->redirect($GLOBALS['ROOT']);
      }
    } else if ($this->authentication->isLoggedIn() && $this->authentication->getCurrentUserId() == $params['id']) {
      $q = new \UserQuery();
      $user = $q->findPK($this->authentication->getCurrentUserId());
      $html = $this->renderer->render('users/edit', array(
        'user' => $user
      ));
      $this->response->setContent($html);
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function update($params)
  {
    if ($this->authentication->isLoggedIn() && $this->authentication->isAdmin() && isset($params['id'])) {
      $enabled = $this->request->getParameter('enabled', false);
      $q = new \UserQuery();
      $user = $q->findPK($params['id']);

      if ($enabled !== $user->getRestaurantEnabled()) {
        $this->mailerService->notifyRestaurantEnabled(array(
          'email' => $user->getEmail()
        ));
      }

      $user->setRestaurantEnabled($enabled);
      $user->save();
      $this->response->redirect($GLOBALS['ROOT'] . "/users/");
    } else if ($this->authentication->isLoggedIn() && $this->authentication->getCurrentUserId() == $params['id']) {
      $q = new \UserQuery();
      $user = $q->findPK($params['id']);
      $errors = [];
      if (!empty($_FILES['image']['name'])) {
        $extsAllowed = array( 'jpg', 'jpeg', 'png' );
        $extUpload = strtolower( substr( strrchr($_FILES['image']['name'], '.') ,1) ) ;
        if (in_array($extUpload, $extsAllowed)) {
          $name = "/uploads/users/{$user->getId()}.$extUpload";
          $user->setImagePath($name);
          $user->save();
          move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT . $name);
        } else {
          //$errors = [
            //"Le estensioni consentite per l'immagine sono jpg, jpeg, png"
          //];
          array_push($errors, "Le estensioni consentite per l'immagine sono jpg, jpeg, png");
        }
      }
      $html = $this->renderer->render('users/edit', array(
        'user' => $user,
        'errors' => $errors
      ));
      $this->response->setContent($html);
    } else {
      $this->response->redirect($GLOBALS['ROOT']);
    }
  }

  public function delete($params)
  {
  }
}
