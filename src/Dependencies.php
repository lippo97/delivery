<?php declare(strict_types = 1);

$injector = new \Auryn\Injector;

$injector->alias('Http\Request', 'Http\HttpRequest');
$injector->share('Http\HttpRequest');
$injector->define('Http\HttpRequest', [
  ':get' => $_GET,
  ':post' => $_POST,
  ':cookies' => $_COOKIE,
  ':files' => $_FILES,
  ':server' => $_SERVER,
]);

$injector->alias('Http\Response', 'Http\HttpResponse');
$injector->share('Http\HttpResponse');

$injector->alias('Delivery\Authentication\Authentication', 'Delivery\Authentication\DatabaseAuthentication');

$injector->alias('Delivery\Mailer\MailerService', 'Delivery\Mailer\MailerServiceImpl');

$injector->delegate('Twig_Environment', function () use ($injector, $root) {
  $loader = new Twig_Loader_Filesystem(dirname(__DIR__) . '/templates');
  $twig = new Twig_Environment($loader);

  $is_logged_in_function = new Twig_SimpleFunction('is_logged_in', function() use ($injector) {
    $authentication= $injector->make('\Delivery\Authentication\Authentication');
    return $authentication->isLoggedIn();
  });

  $is_admin_function = new Twig_SimpleFunction('is_admin', function() use ($injector) {
    $authentication = $injector->make('\Delivery\Authentication\Authentication');
    return $authentication->isAdmin();
  });

  $is_restaurant_function = new Twig_SimpleFunction('is_restaurant', function() use ($injector) {
    $authentication = $injector->make('\Delivery\Authentication\Authentication');
    return $authentication->isRestaurant();
  });

  $get_current_user_id_function = new Twig_SimpleFunction('get_current_user_id', function() use ($injector) {
    $authentication = $injector->make('\Delivery\Authentication\Authentication');
    return $authentication->getCurrentUserId();
  });

  $get_current_user_email_function = new Twig_SimpleFunction('get_current_user_email', function() use ($injector) {
    $authentication = $injector->make('\Delivery\Authentication\Authentication');
    return $authentication->getCurrentUserEmail();
  });

  $get_current_user_restaurant_name_function = new Twig_SimpleFunction('get_current_user_restaurant_name', function() use ($injector) {
    $authentication = $injector->make('\Delivery\Authentication\Authentication');
    return $authentication->getCurrentUserRestaurantName();
  });

  $link_function = new Twig_SimpleFunction('link_to', function($path) use ($root) {
    $link_to = require('LinkTo.php');
    return $link_to($GLOBALS['ROOT'], $path);
  });

  $twig->addFunction($is_logged_in_function);
  $twig->addFunction($is_admin_function);
  $twig->addFunction($is_restaurant_function);
  $twig->addFunction($get_current_user_id_function);
  $twig->addFunction($get_current_user_email_function);
  $twig->addFunction($get_current_user_restaurant_name_function);
  $twig->addFunction($link_function);
  return $twig;
});
$injector->alias('Delivery\Template\Renderer', 'Delivery\Template\TwigRenderer');

return $injector;
