# Delivery

## Setup

### Easy way

Run:

```bash
git clone git@bitbucket.org:lippo97/delivery.git
cd delivery
./setup [-f]
```

### Do it by hand

First clone the repository.

```bash
git clone git@bitbucket.org:lippo97/delivery.git
```

Then download the project dependencies.

```bash
composer install
```

Setup a database account accordingly to `database/propel.yaml`. Then run:

```bash
database/propel sql:build --config-dir=database --schema-dir=database --output-dir=database/generated-sql --overwrite
database/propel model:build --config-dir=database --schema-dir=database --output-dir=database/generated-classes
database/propel sql:insert --config-dir=database --sql-dir=database/generated-sql
database/propel config:convert --config-dir=database --output-dir=database/generated-conf

```

Finally include generated classes in `database/generated-classes` to `vendor/autoload.php` running:

```bash
composer dump-autoload
```

Optionally you can fill the database with fixture data.

```bash
php database/fill-database.php
```


## Credits

This project was developed by Filippo Nardini and Luca Sambuchi.
